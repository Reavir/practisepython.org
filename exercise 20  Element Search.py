# exercise 20
# Write a function that takes an ordered list of numbers
# (a list where the elements are in order from smallest to largest)
# and another number. The function decides whether or not the given number is
# inside the list and returns (then prints) an appropriate boolean.


def bin_search(lis, num):
    find = False
    while not find:
        print(lis)
        if len(lis) % 2 == 0:
            if lis[int(len(lis)/2)] < num:
                lis = lis[int(len(lis)/2):len(lis)]
            elif lis[int(len(lis)/2)] > num:
                lis = lis[0:int(len(lis)/2)]
            elif len(lis) == 1:
                find = True
                print(num, "is not in the list")
            elif lis[int(len(lis)/2)] == num:
                find = True
                print(num, "is in the list")

        elif len(lis) % 2 != 0:
            if lis[int(len(lis) / 2)] == num:
                find = True
                print(num, "is in the list")
            elif len(lis) == 1:
                find = True
                print(num, "is not in the list")
            elif lis[int(len(lis)/2)] != num:
                del lis[int(len(lis)/2)]
                if lis[int(len(lis) / 2)] < num:
                    lis = lis[int(len(lis) / 2):len(lis)]
                elif lis[int(len(lis) / 2)] > num:
                    lis = lis[0:int(len(lis) / 2)]
                elif len(lis) == 1:
                    find = True
                    print(num, "is not in the list")
                elif lis[int(len(lis)/2)] == num:
                    find = True
                    print(num, "is in the list")


lista = [4,5,6,7]
numer = 1

bin_search(lista,numer)
